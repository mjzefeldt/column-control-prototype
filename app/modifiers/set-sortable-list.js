// documentation for ember-modifier https://github.com/ember-modifier/ember-modifier
// documentation for base library options https://github.com/SortableJS/Sortable
// documentation for autoscroll plugin options https://github.com/SortableJS/Sortable/tree/master/plugins/AutoScroll

import Modifier from 'ember-modifier';
import Sortable from 'sortablejs/modular/sortable.complete.esm.js';

export default class SetSortableList extends Modifier {
  sortableList = null;

  didReceiveArguments() {
    const sortableSettings = {};

    // sortableJS autoscroll plugin options
    if (this.args.named.scroll) {
      sortableSettings.scroll = this.args.named.scroll;
    }
    if (this.args.named.forceAutoscrollFallback) {
      sortableSettings.forceAutoscrollFallback =
        this.args.named.forceAutoscrollFallback;
    }
    if (this.args.named.scrollFn) {
      sortableSettings.scrollFn = this.args.named.scrollFn;
    }
    if (this.args.named.scrollSensitivity) {
      sortableSettings.scrollSensitivity = this.args.named.scrollSensitivity;
    }
    if (this.args.named.scrollSpeed) {
      sortableSettings.scrollSpeed = this.args.named.scrollSpeed;
    }
    if (this.args.named.bubbleScroll) {
      sortableSettings.bubbleScroll = this.args.named.bubbleScroll;
    }

    // sortableJS base library options
    if (this.args.named.groupName) {
      sortableSettings.group = this.args.named.groupName;
    }
    if (this.args.named.sort) {
      sortableSettings.sort = this.args.named.sort;
    }
    if (this.args.named.delay) {
      sortableSettings.delay = this.args.named.delay;
    }
    if (this.args.named.delayOnTouchOnly) {
      sortableSettings.delayOnTouchOnly = this.args.named.delayOnTouchOnly;
    }
    if (this.args.named.touchStartThreshold) {
      sortableSettings.touchStartThreshold = this.args.touchStartThreshold;
    }
    if (this.args.named.disabled) {
      sortableSettings.disabled = this.args.named.disabled;
    }
    if (this.args.named.store) {
      sortableSettings.store = this.args.named.store;
    }
    if (this.args.named.animation) {
      sortableSettings.animation = this.args.named.animation;
    }
    if (this.args.named.easing) {
      sortableSettings.easing = this.args.named.easing;
    }
    if (this.args.named.handle) {
      sortableSettings.handle = this.args.named.handle;
    }
    if (this.args.named.filter) {
      sortableSettings.filter = this.args.named.filter;
    }
    if (this.args.named.preventOnFilter) {
      sortableSettings.preventOnFilter = this.args.named.preventOnFilter;
    }
    if (this.args.named.draggable) {
      sortableSettings.draggable = this.args.named.draggable;
    }
    if (this.args.named.dataIdAttr) {
      sortableSettings.dataIdAttr = this.args.named.dataIdAttr;
    }
    if (this.args.named.ghostClass) {
      sortableSettings.ghostClass = this.args.named.ghostClass;
    }
    if (this.args.named.chosenClass) {
      sortableSettings.chosenClass = this.args.named.chosenClass;
    }
    if (this.args.named.dragClass) {
      sortableSettings.dragClass = this.args.named.dragClass;
    }
    if (this.args.named.swapThreshold) {
      sortableSettings.swapThreshold = this.args.named.swapThreshold;
    }
    if (this.args.named.invertSwap) {
      sortableSettings.invertSwap = this.args.named.invertSwap;
    }
    if (this.args.named.invertedSwapThreshold) {
      sortableSettings.invertedSwapThreshold =
        this.args.named.invertedSwapThreshold;
    }
    if (this.args.named.direction) {
      sortableSettings.direction = this.args.named.direction;
    }
    if (this.args.named.forceFallback) {
      sortableSettings.forceFallback = this.args.named.forceFallback;
    }
    if (this.args.named.fallbackClass) {
      sortableSettings.allbackClass = this.args.named.fallbackClass;
    }
    if (this.args.named.fallbackOnBody) {
      sortableSettings.fallbackOnBody = this.args.named.fallbackOnBody;
    }
    if (this.args.named.fallbackTolerance) {
      sortableSettings.fallbackTolerance = this.args.named.fallbackTolerance;
    }
    if (this.args.named.dragoverBubble) {
      sortableSettings.dragoverBubble = this.args.named.dragoverBubble;
    }
    if (this.args.named.removeCloneOnHide) {
      sortableSettings.removeCloneOnHide = this.args.named.removeCloneOnHide;
    }
    if (this.args.named.emptyInsertThreshold) {
      sortableSettings.emptyInsertThreshold =
        this.args.named.emptyInsertThreshold;
    }
    if (this.args.named.setData) {
      sortableSettings.setData = (dataTransfer, dragEl) => {
        this.args.named.setData(dataTransfer, dragEl);
      };
    }
    if (this.args.named.onChoose) {
      sortableSettings.onChoose = (evt) => {
        this.args.named.onChoose(evt);
      };
    }
    if (this.args.named.onStart) {
      sortableSettings.onStart = (evt) => {
        this.args.named.onStart(evt);
      };
    }
    if (this.args.named.onEnd) {
      sortableSettings.onEnd = (evt) => {
        this.args.named.onEnd(evt);
      };
    }
    if (this.args.named.onAdd) {
      sortableSettings.onAdd = (evt) => {
        this.args.named.onAdd(evt);
      };
    }
    if (this.args.named.onUpdate) {
      sortableSettings.onUpdate = (evt) => {
        this.args.named.onUpdate(evt);
      };
    }
    if (this.args.named.onSort) {
      sortableSettings.onSort = (evt) => {
        this.args.named.onSort(evt);
      };
    }
    if (this.args.named.onRemove) {
      sortableSettings.onRemove = (evt) => {
        this.args.named.onRemove(evt);
      };
    }
    if (this.args.named.onFilter) {
      sortableSettings.onFilter = (evt) => {
        this.args.named.onFilter(evt);
      };
    }
    if (this.args.named.onMove) {
      sortableSettings.onMove = (evt, originalEvent) => {
        this.args.named.onMove(evt, originalEvent);
      };
    }
    if (this.args.named.onClone) {
      sortableSettings.onClone = (evt) => {
        this.args.named.onClone(evt);
      };
    }
    if (this.args.named.onChange) {
      sortableSettings.onChange = (evt) => {
        this.args.named.onChange(evt);
      };
    }

    // create sortable list
    if (this.element) {
      this.sortableList = new Sortable(this.element, sortableSettings);
    }
  }

  willDestroy() {
    this.sortableList.destroy();
  }
}
