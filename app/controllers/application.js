import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { TrackedArray } from 'tracked-built-ins';

export default class ApplicationController extends Controller {
  @tracked _dragDropIsOpen = false;
  @tracked _sortJsIsOpen = false;

  @action
  toggleDragDropIsOpen() {
    this._dragDropIsOpen = !this._dragDropIsOpen;
    if (this._sortJsIsOpen) {
      this._sortJsIsOpen = false;
    }
  }

  @action
  toggleSortJsIsOpen() {
    this._sortJsIsOpen = !this._sortJsIsOpen;
    if (this._dragDropIsOpen) {
      this._dragDropIsOpen = false;
    }
  }

  @tracked _columns = new TrackedArray([
    { name: 'Mario', id: 1, isFixed: 'left', isVisible: true, subcolumns: [] },
    {
      name: 'Toad',
      id: 3,
      isFixed: 'left',
      isVisible: true,
      subcolumns: new TrackedArray([
        {
          name: 'Red Toad',
          id: 7,
          isFixed: '',
          isVisible: true,
          subcolumns: [],
        },
        {
          name: 'Green Toad',
          id: 8,
          isFixed: '',
          isVisible: true,
          subcolumns: [],
        },
      ]),
    },
    { name: 'Yoshi', id: 6, isFixed: 'left', isVisible: true, subcolumns: [] },
    { name: 'Luigi', id: 2, isFixed: '', isVisible: true, subcolumns: [] },
    { name: 'Peach', id: 4, isFixed: '', isVisible: true, subcolumns: [] },
    {
      name: 'Bowser',
      id: 5,
      isFixed: '',
      isVisible: true,
      subcolumns: new TrackedArray([
        {
          name: 'Blue Bowser',
          id: 9,
          isFixed: '',
          isVisible: true,
          subcolumns: [],
        },
        {
          name: 'Pink Bowser',
          id: 10,
          isFixed: '',
          isVisible: true,
          subcolumns: [],
        },
      ]),
    },
  ]);

  get columns() {
    return this._columns.filter((col) => col.isVisible);
  }

  set columns(newColumnsOrder) {
    console.log('setter firing', newColumnsOrder);
    this._columns = newColumnsOrder;
  }
}
