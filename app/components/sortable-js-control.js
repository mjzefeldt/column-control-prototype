import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
// import Sortable from 'sortablejs/modular/sortable.complete.esm.js';
import { TrackedArray } from 'tracked-built-ins';

export default class SortableJsControlComponent extends Component {
  // not doing anything with these - but have references if need them
  // frozenLeftColumnsList = null;
  // bodyColumnsList = null;
  // frozenRightColumnsList = null;
  // hiddenColumnsList = null;

  // what if use a hash so have key and then also tracked array?

  fullColumnList = new Map([
    ['frozenLeftColumns', new TrackedArray([])],
    ['bodyColumns', new TrackedArray([])],
    ['frozenRightColumns', new TrackedArray([])],
    ['hiddenColumns', new TrackedArray([])],
  ]);

  @tracked scrollElement = document.getElementById('control-container');

  // listElementIds = [
  //   'frozenLeftColumns',
  //   'bodyColumns',
  //   'frozenRightColumns',
  //   'hiddenColumns',
  // ];

  // use tracked properties to populate buckets instead of getters as causes double render in sorterjs object otherwise
  // @tracked frozenLeftColumns = [];
  // @tracked bodyColumns = [];
  // @tracked frozenRightColumns = [];
  // @tracked hiddenColumns = [];

  constructor(owner, args) {
    super(owner, args);
    const frozenLeftColumn = (col) => col.isFixed == 'left' && col.isVisible;
    const bodyColumn = (col) => col.isFixed == '' && col.isVisible;
    const frozenRightColumn = (col) => col.isFixed == 'right' && col.isVisible;
    const hiddenColumn = (col) => col.isVisible == false;
    const filterColumnFunc = (func) => {
      return this.args.columns.filter((col) => func(col));
    };

    // this.frozenLeftColumns = filterColumnFunc(frozenLeftColumn);
    // this.bodyColumns = filterColumnFunc(bodyColumn);
    // this.frozenRightColumns = filterColumnFunc(frozenRightColumn);
    // this.hiddenColumns = filterColumnFunc(hiddenColumn);
    this.fullColumnList.set(
      'frozenLeftColumns',
      filterColumnFunc(frozenLeftColumn)
    );
    this.fullColumnList.set('bodyColumns', filterColumnFunc(bodyColumn));
    this.fullColumnList.set(
      'frozenRightColumns',
      filterColumnFunc(frozenRightColumn)
    );
    this.fullColumnList.set('hiddenColumns', filterColumnFunc(hiddenColumn));
  }

  // SortableJS requires more direct DOM manipuation to update tracked sortable lists.
  // Must leverage onDidInsert to make this library work as need relevant DOM elements inserted to wrap with SortableJS.
  // @action
  // onDidInsert() {
  // const sortableSettings = {
  //   scroll: document.getElementById('control-container'),
  //   scrollSensitivity: 80,
  //   group: 'sortable-lists',
  //   fallbackOnBody: true,
  //   // swapThreshold: 0.65,
  //   onAdd: (evt) => {
  //     this.updateOrder(
  //       evt.oldIndex,
  //       evt.newIndex,
  //       evt.from,
  //       evt.to,
  //       evt.item
  //     );
  //   },
  //   onUpdate: (evt) => {
  //     this.updateOrder(
  //       evt.oldIndex,
  //       evt.newIndex,
  //       evt.from,
  //       evt.to,
  //       evt.item
  //     );
  //   },
  // };
  // this.frozenLeftColumnsList = Sortable.create(
  //   document.getElementById('frozenLeftColumns'),
  //   sortableSettings
  // );
  // this.bodyColumnsList = Sortable.create(
  //   document.getElementById('bodyColumns'),
  //   sortableSettings
  // );
  // this.frozenRightColumnsList = Sortable.create(
  //   document.getElementById('frozenRightColumns'),
  //   sortableSettings
  // );
  // this.hiddenColumnsList = Sortable.create(
  //   document.getElementById('hiddenColumns'),
  //   sortableSettings
  // );
  // // get list of nestedSortables
  // const nestedSortables = [].slice.call(
  //   document.querySelectorAll('.nested-sortable')
  // );
  // // Loop through each nested sortable element
  // for (var i = 0; i < nestedSortables.length; i++) {
  //   new Sortable(nestedSortables[i], {
  //     scroll: document.getElementById('control-container'),
  //     scrollSensitivity: 80,
  //     group: 'unique-list',
  //     // fallbackOnBody: true,
  //     // swapThreshold: 0.65,
  //     onUpdate: (evt) => {
  //       this.updateOrder(
  //         evt.oldIndex,
  //         evt.newIndex,
  //         evt.from,
  //         evt.to,
  //         evt.item
  //       );
  //     },
  //   });
  // }
  // }

  // Get tracked sortable lists and use fullColumnList dictionary with SortableJS passed in event arguments to get:
  // 1) tracked column list that column item was moved from
  // 2) tracked column list that column item was moved to
  // 3) TableColumn object (element) that corresponds to column item
  // Update isFixed and isVisible properties on TableColumn object (element) according to new bucket element is placed into
  // Update column ordering within tracked sortable lists by removing/adding element with index references from SortableJS event arguments.
  // Spread tracked sortable lists together into single array and pass into @onChange arg hook.
  @action
  updateOrder({ oldIndex, newIndex, from, to, item }) {
    console.log(oldIndex, newIndex, from, to, item);
    // create full column dict to help reference tracked lists
    // const fullColumnList = {
    //   frozenLeftColumns: this.frozenLeftColumns,
    //   bodyColumns: this.bodyColumns,
    //   frozenRightColumns: this.frozenRightColumns,
    //   hiddenColumns: this.hiddenColumns,
    // };
    const oldListName = from.id;
    const newListName = to.id;
    let oldList;
    let newList;

    // make oldList and newList assignment below DRY

    if (this.fullColumnList.has(oldListName)) {
      oldList = this.fullColumnList.get(oldListName);
    } else {
      // ... handle something that falls outside this by accident?
      // find subcolumns parent list
      const parentListName = from.dataset.parentList;
      // find subcolumns parent item and set oldList to subcolumns list
      const parentItemId = from.dataset.parentItem;
      oldList = this.fullColumnList
        .get(parentListName)
        .find((col) => col.id == parentItemId).subcolumns;
    }

    if (this.fullColumnList.has(newListName)) {
      newList = this.fullColumnList.get(newListName);
    } else {
      // ... handle something that falls outside this by accident?
      // find subcolumns parent list
      const parentListName = to.dataset.parentList;
      // find subcolumns parent item and set oldList to subcolumns list
      const parentItemId = to.dataset.parentItem;
      newList = this.fullColumnList
        .get(parentListName)
        .find((col) => col.id == parentItemId).subcolumns;
    }

    const element = oldList[oldIndex];

    if (element) {
      // set isFixed property
      if (newListName == 'frozenLeftColumns') {
        element.isFixed = 'left';
      } else if (newListName == 'frozenRightColumns') {
        element.isFixed = 'right';
      } else {
        element.isFixed = '';
      }

      // set isVisible property
      element.isVisible = newListName == 'hiddenColumns' ? false : true;

      // if element has subcolumn list - set subcolumn list ul element new data-parent-list
      if (element.subcolumns.length > 0) {
        const subColumnListEl = document.getElementById(
          `${item.id}-subcolumns`
        );
        subColumnListEl.dataset.parentList = newListName;
      }

      // set ordering
      oldList.splice(oldIndex, 1);
      newList.splice(newIndex, 0, element);
    } else {
      console.error('element for moved column not found');
    }

    // this.args.onChange(
    //   Object.keys(this.fullColumnList)
    //     .map((key) => this.fullColumnList[key])
    //     .flat()
    // );

    // this would need to be changed if getting from a map
    this.args.onChange([...this.fullColumnList.values()].flat());
  }
}

// taking in array of columnObjects
// they are assigned to their buckets based on
// isFixed value
// order within array
// isVisible value
// limit parent/child column property setting: isFixed and isVisible set on parent only. Can re-order within child.

// populate list with name and id for column
// when column moves (tracking id), update passed in array via setter through @onChange.
// update ordering within array (likely a type of splice operation with event.oldIndex event.newIndex )
// update isFixed value on column with matching id by looking if moved to / away from fixed bucket (event.from event.to)
// same approach for isVisible as isFixed
