import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class DragDropControlComponent extends Component {
  @tracked frozenLeftColumns = [];
  @tracked bodyColumns = [];
  @tracked frozenRightColumns = [];
  @tracked hiddenColumns = [];

  constructor(owner, args) {
    super(owner, args);
    const frozenLeftColumn = (col) => col.isFixed == 'left' && col.isVisible;
    const bodyColumn = (col) => col.isFixed == '' && col.isVisible;
    const frozenRightColumn = (col) => col.isFixed == 'right' && col.isVisible;
    const hiddenColumn = (col) => col.isVisible == false;
    const filterColumnFunc = (func) => {
      return this.args.columns.filter((col) => func(col));
    };

    this.frozenLeftColumns = filterColumnFunc(frozenLeftColumn);
    this.bodyColumns = filterColumnFunc(bodyColumn);
    this.frozenRightColumns = filterColumnFunc(frozenRightColumn);
    this.hiddenColumns = filterColumnFunc(hiddenColumn);
  }

  @action
  updateOrder(event) {
    console.log(event, '<<< event');
    // ember-drag-drop components mutate passed in tracked lists directly as column items are reordered.
    // So, just need to put array back together and send back into @onChange argument.

    // Before pass back to @onChange, first update isFixed and isVisible according to which list column item moved into.
    // With ember-drag-drop, can't reference the specific moved column item because not given context for lists or indices.
    // To update isFixed and isVisible, set property values for each TableColumn object in respective buckets lists en masse.
    // Inefficient, but works.

    this.frozenLeftColumns.forEach((col) => {
      col.isFixed = 'left';
      col.isVisible = true;
    });
    this.bodyColumns.forEach((col) => {
      col.isFixed = '';
      col.isVisible = true;
    });
    this.frozenRightColumns.forEach((col) => {
      col.isFixed = 'right';
      col.isVisible = true;
    });
    this.hiddenColumns.forEach((col) => {
      col.isVisible = false;
    });

    this.args.onChange([
      ...this.frozenLeftColumns,
      ...this.bodyColumns,
      ...this.frozenRightColumns,
      ...this.hiddenColumns,
    ]);
  }

  @action
  updateObject(evt) {
    // console.log(evt, '<<< firing updateObject');
  }
}

// main issue - if have empty list, can no longer drop into list.
// this is the case in demos as well
// it's like there is no target area for drop
// https://github.com/mharris717/ember-drag-drop/issues/192
